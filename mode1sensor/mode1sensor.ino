#define PIN_TRIGGER 2
#define PIN_ECHO 3

#include "NewPing.h"
#include "math.h"

#define MAX_DISTANCE 210
#define OUTLIER_THRESHOLD 0.1 // values must be 30% different to be considered non-noise

NewPing sonar = NewPing(PIN_TRIGGER, PIN_ECHO, MAX_DISTANCE);
// NewPing sonar = NewPing(PIN_TRIGGER, PIN_ECHO, MAX_DISTANCE);
unsigned int old_value = 0;

bool is_outlier(unsigned int new_value, unsigned int old_value) {
    double rate = double (new_value) / old_value;
    return abs(rate - 1) >= OUTLIER_THRESHOLD;
}

void setup() {
    Serial.begin(9600);
}

void loop() {
    unsigned long median_ping = sonar.ping_median(20, MAX_DISTANCE); //~200ms
    unsigned int distance_cm = sonar.convert_cm(median_ping);
    Serial.print("Measured : ");
    Serial.print(distance_cm); 
    Serial.print(" cm");

    if (distance_cm == 0) {
        Serial.print(". Object too close or too far.\n");
        return;
    } else {
        Serial.println("");
    }    
    // if (old_value != 0) {    
    //     if (is_outlier(distance_cm, old_value)) {
    //         Serial.print("\nDistance: ~");
    //         Serial.print(abs((int) distance_cm - (int) old_value));
    //         Serial.println(" cm\n");
    //     } else {
    //         Serial.println(", no changes found.\n");
    //     }
    // } else 
    //     Serial.println("\n");
    // old_value = distance_cm;
}
