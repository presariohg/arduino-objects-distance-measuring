#define PIN_TRIGGER_1 2
#define PIN_ECHO_1 3

#define PIN_TRIGGER_2 4
#define PIN_ECHO_2 5

#include "NewPing.h"
#include "math.h"
#include "quicksort.h"

#define MAX_DISTANCE 210
#define SENSORS_DISTANCE 100 // distacne between 2 sensors are 100 cm
#define OUTLIER_THRESHOLD 0.2 // values must be 20% different to be considered non-noise
#define SAMPLE_SIZE 50 // 200ms between pool
#define ALERT_DISTANCE 100

#define now_us() micros()

NewPing sonar1 = NewPing(PIN_TRIGGER_1, PIN_ECHO_1, MAX_DISTANCE);
NewPing sonar2 = NewPing(PIN_TRIGGER_2, PIN_ECHO_2, MAX_DISTANCE);
unsigned int sensor_1_samples[SAMPLE_SIZE];
unsigned int sensor_2_samples[SAMPLE_SIZE];


bool is_outlier(unsigned int new_value, unsigned int old_value) {
    double rate = double (new_value) / old_value;
    return abs(rate - 1) >= OUTLIER_THRESHOLD;
}


int do_pythagorean_magic(int sensor_data_1, int sensor_data_2) {
    int leg1 = abs(sensor_data_1 - sensor_data_2);
    int leg2 = SENSORS_DISTANCE;

    return sqrt(leg1 * leg1 + leg2 * leg2); // calculate the hypotenuse
}

unsigned int get_latest_ping(NewPing sensor) {
    unsigned int result = sensor.ping(MAX_DISTANCE);

    // while (result == NO_ECHO) { // ping again until get a valid value
    //     result = sensor.ping(MAX_DISTANCE);
    // }

    return result;
}

void setup() {
    Serial.begin(9600);
    for (int i = 0; i < SAMPLE_SIZE; i++) {
        sensor_1_samples[i] = 0;
        sensor_2_samples[i] = 0;
    }
}

void print_sensor(char* sensor_name, int sensor_data) {
    Serial.print(sensor_name);
    if (sensor_data == 0) {
        Serial.println(" N/A. Object out of detection range");
    } else {
        if (sensor_data <= ALERT_DISTANCE)
            Serial.print(" CAUTION!!: ");
        else 
            Serial.print(": ");
        Serial.print(sensor_data);
        Serial.println("cm");
    }    
}

void loop() {
    // int i = 0;
    // while (i < SAMPLE_SIZE) {
    //     int start = now_us();
    //     sensor_1_samples[i] = get_latest_ping(sonar1);
    //     sensor_2_samples[i] = get_latest_ping(sonar2);
    //     i++;
    //     if ((i < SAMPLE_SIZE) &&
    //         ((now_us() - start) < PING_MEDIAN_DELAY))
    //         delay((PING_MEDIAN_DELAY + start - now_us()) / 1000); // us to ms
    // }

    // quick_sort(sensor_1_samples, 0, SAMPLE_SIZE - 1);
    // quick_sort(sensor_2_samples, 0, SAMPLE_SIZE - 1);

    // unsigned int sensor_1_median = sensor_1_samples[SAMPLE_SIZE >> 1];
    // unsigned int sensor_2_median = sensor_2_samples[SAMPLE_SIZE >> 1];

    // int sensor_data_1 = sonar1.convert_cm(sensor_1_median);
    // int sensor_data_2 = sonar2.convert_cm(sensor_2_median);
    int sensor_data_1 = sonar1.convert_cm(sonar1.ping_median(15, MAX_DISTANCE));
    int sensor_data_2 = sonar2.convert_cm(sonar2.ping_median(15, MAX_DISTANCE));
    
    print_sensor("Sensor 1", sensor_data_1);
    print_sensor("Sensor 2", sensor_data_2);

    // if (is_outlier(sensor_data_1, sensor_data_2)) { // 2 sensors detecting 2 different objects
    //     int distance = do_pythagorean_magic(sensor_data_1, sensor_data_2);

    //     Serial.print("Distance: ~");
    //     Serial.println(distance);
    // } else {
    //     Serial.println("No changes found.");
    // }
    Serial.println();
}
